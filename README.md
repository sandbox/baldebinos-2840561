CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting

INTRODUCTION
------------

The Flowplayer HTML5 Video module allows you to display video files hosted in your server.
It creates a Field Formatter, available for fields of type File.

Supports:

 * mp4, ogg and webm as video formats.
 * vtt as subtitles format.
 * gif or jpg as images for the poster.

Basic features:

 * Setting Width and Height of the player.
 * Optional custom CSS Classes.
 * Setting Preload.
 * Setting Autoplay.
 * Setting Muted.
 * Usage of subtitles (.vtt).
   * If using subtitles it is recommended to "Enable Description field" option.
   * The content of this field will be used for displaying the language of each subtitle.
 * Usage of poster (.jpg or .gif).
   * Although most browsers will show the first frame of the video before starting playing, some mobile browsers will not.
   * To avoid the player to initially render blank in some platforms, uploading a gif or jpg image for use as poster is recommended.
   * If you upload several images only the top one will be used for the poster. Rearrange the order of the files to select the image to use.

Flowplayer specific features:

 * Load Flowplayer from cdn (default), local or disable.
   * If use Flowplayer is disabled, the module will not use java scripts or CSS to render the video, and depends on the capability of the browser to render HTML5 Video.
 * Choose from several skins as provided by Flowplayer.
 * Video ratio. Options are:
   * 16:9 - landscape
   * 4:3 - landscape
   * 12:5 - landscape
   * 9:16 - portrait
   * 3:4 - portrait
 * Playlist.
   * If several videos where uploaded, this option allows to create links below the player to switch among them. It uses the Description field to render the link.
 * Video description contains.
   * If set to text, the playlist is shown as text links.
   * If set to url, the playlist is shown as image links.
     * In this case you must provide in the video description a valid url to an image.
 * Playlist advance.
   * If set to true, plays the next clip when the current one is finished and stops when last clip ends.

REQUIREMENTS
------------

This module requires:

 * Flowplayer java script and CSS.
   * By default, uses the free versions of flowplayer.min.js and all-skins.css loaded from releases.flowplayer.org CDN.
   * If you prefer to load the files from your own server, namely if you buy a Flowplayer license, set Use Flowplayer from to local.
     * For your convenience a copy of Flowplayer is included with this module, in case loading Flowplayer from your server is required.
     * Currently, the included version of Flowplayer is 6.0.5.
   * For detailed information about Flowplayer, visit the documentation at flowplayer.org.
 * core/jquery
 
INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module.

CONFIGURATION
-------------

 * Go to Structure -> Content types, and choose Add content type (or use an existing one).
 * For that Content type choose Manage fields.
 * Choose Add field.
 * In Select a field type, choose File.
 * Enter an appropriate Label. Example: Video.
 * As Allowed number of values, type the required number or choose Unlimited.
 * As Allowed file extensions, type the extensions you plan to use. Example: mp4, vtt, jpg.
   * If you enable and upload files with extensions other then mp4, ogg, webm, vtt, gif or jpg, they will be ignored.
 * Check the "Enable Description field" option. The Description field will be used to identify the language of the subtitles.
   * When uploading vtt files, use the Description to identify the language of the subtitle. Example: Português.
 * On the Manage display tab, under FORMAT, choose Flowplayer HTML5 Video for the newly created field.
 * Optionally, you can:
   * Set how to load Flowplayer (cdn, local or disable).
   * Set the Width and Height of the player.
     * Width and Height are required and must specify the value and units or use auto.
   * Set the Skin to use if the player, as provided by Flowplayer.
   * Set the Ratio to better adjust the player size.
   * Enable the use of Playliest.
     * If several videos where uploaded, this option allows you to create links below the player to switch among them.
   * Set Video description contains.
     * If set to text, the playlist is shown as text links.
     * If set to url, the playlist is shown as image links.
       * In this case you must provide in the video description a valid url to an image.
     * It requires Playliest to be enabled.
   * Set Playlist advance.
     * If set to true, plays the next clip when the current one is finished and stops when last clip ends.
     * It requires Playliest to be enabled.
   * Specify custom CSS Classes of the player.
     * CSS Classes is optional. Check your theme documentation for the appropriate classes to use.
   * You can also set the behavior of the player by setting Preload, Autoplay and Muted.
 * Now, add new content to your site and have fun.

TROUBLESHOOTING
---------------

 * Video files may be very large. You may need to change the php.ini file to allow uploading such large files.
 * Set post_max_size, upload_max_filesize and max_file_uploads accordingly to your needs.
