<?php

/**
 * @file
 * Contains \Drupal\flowplayer_html5_video\Plugin\Field\FieldFormatter\FlowplayerHTML5VideoFormatter.
 */

namespace Drupal\flowplayer_html5_video\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'flowplayer_html5_video' formatter.
 *
 * @FieldFormatter(
 *   id = "flowplayer_html5_video_formater",
 *   label = @Translation("Flowplayer HTML5 Video"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */

class FlowplayerHTML5VideoFormatter extends FlowplayerHTML5VideoFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'flowplayer' => 'cdn',
      'width' => '100%',
      'height' => 'auto',
      'skin' => 'minimalist',
      'ratio' => '0.5625',
      'playlist' => FALSE,
      'videodesccontains' => 'text',
      'advance' => "false",
      'class' => '',
      'preload' => 'auto',
      'autoplay' => FALSE,
      'muted' => FALSE,
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['flowplayer'] = [
      '#title' => t('Use Flowplayer from'),
      '#type' => 'select',
      '#options' => array('cdn' => 'cdn', 'local' => 'local', 'disabled' => 'disabled'),
      '#default_value' => $this->getSetting('flowplayer'),
      '#description' => t('Use Flowplayer. Options are:
                           <ul>
                             <li>Load Flowplayer from CDN = cdn</li>
                             <li>Load Flowplayer from local host = local</li>
                             <li>Do not use Flowplayer = disabled</li>
                           </ul>
                           The default value is cdn.<br />
                           If disabled, the video will be rendered expecting the capability of the browser to render HTML5 Video'),
    ];
    $element['width'] = [
      '#title' => t('Width'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('width'),
      '#required' => TRUE,
      '#description' => t('The width of the player. Value and units must be specified or use auto.<br />
                           The default value is 100%.'),
    ];
    $element['height'] = [
      '#title' => t('Height'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('height'),
      '#required' => TRUE,
      '#description' => t('The height of the player. Value and units must be specified or use auto.<br />
                           The default value is auto.'),
    ];
    $element['skin'] = [
      '#title' => t('Skin'),
      '#type' => 'select',
      '#options' => array('minimalist' => 'minimalist', 'functional' => 'functional', 'playful' => 'playful'),
      '#default_value' => $this->getSetting('skin'),
      '#description' => t('The skin for Flowplayer.<br />
                           The default value is minimalist.<br />
                           If Flowplayer is not enabled, this will be ignored.'),
    ];
    $element['ratio'] = [
      '#title' => t('Ratio'),
      '#type' => 'select',
      '#options' => array('0.5625' => '16:9', '0.75' => '4:3', '0.4167' => '12:5', '1.1778' => '9:16', '1.3333' => '3:4'),
      '#default_value' => $this->getSetting('ratio'),
      '#description' => t('The ratio for Flowplayer. Options are:
                           <ul>
                             <li>16:9 - landscape = 0.5625</li>
                             <li>4:3 - landscape = 0.75</li>
                             <li>12:5 - landscape = 0.4167</li>
                             <li>9:16 - portrait = 1.1778</li>
                             <li>3:4 - portrait = 1.3333</li>
                           </ul>
                           The default value is 16:9.<br />
                           If Flowplayer is not enabled, this will be ignored.'),
    ];
    $element['playlist'] = [
      '#title' => t('Playlist'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('playlist'),
      '#description' => t('If several videos where uploaded, this option allows you to create links below the player to switch among them.<br />
                           It uses the Description field to render the link.<br />
                           The default value is disabled.<br />
                           If Flowplayer is not enabled, this will be ignored.'),
    ];
    $element['videodesccontains'] = [
      '#title' => t('Video description contains'),
      '#type' => 'select',
      '#options' => array('text' => 'text', 'url' => 'url'),
      '#default_value' => $this->getSetting('videodesccontains'),
      '#description' => t('If set to text, the playlist is shown as text links.<br />
                           If set to url, the playlist is shown as image links.<br />
                           <ul>
                             <li>In this case you must provide in the video description a valid url to an image.</li>
                           </ul>
                           The default value is text.<br />
                           It requires playlist to be enabled.<br />
                           If Flowplayer is not enabled, this will be ignored.'),
    ];
    $element['advance'] = [
      '#title' => t('Playlist advance'),
      '#type' => 'select',
      '#options' => array('false' => 'false', 'true' => 'true'),
      '#default_value' => $this->getSetting('advance'),
      '#description' => t('If set to true, plays the next clip when the current one is finished and stops when last clip ends.<br />
                           The default value is false.<br />
                           It requires playlist to be enabled.<br />
                           If Flowplayer is not enabled, this will be ignored.'),
    ];
    $element['class'] = [
      '#title' => t('CSS Classes'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('class'),
      '#required' => FALSE,
      '#description' => t('Optional custom CSS classes for the player.<br />
                           Check your theme documentation for the appropriate classes.'),
    ];
    $element['preload'] = [
      '#title' => t('Preload'),
      '#type' => 'select',
      '#options' => array('auto' => 'auto', 'metadata' => 'metadata', 'none' => 'none'),
      '#default_value' => $this->getSetting('preload'),
      '#description' => t('How the player loads data. Options are:
                           <ul>
                             <li>Load the entire video when the page loads = auto</li>
                             <li>Load only metadata when the page loads = metadata</li>
                             <li>NOT load the video when the page loads = none</li>
                           </ul>
                           If set to none you may experience problems with some browsers.<br />
                           This attribute may be ignored by some browsers.<br />
                           The preload attribute is ignored if autoplay is present.<br />
                           The default value is auto.'),
    ];
    $element['autoplay'] = [
      '#title' => t('Autoplay'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('autoplay'),
    ];
    $element['muted'] = [
      '#title' => t('Muted'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('muted'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $summary[] = t('Width: @width / Height: @height / Skin: @skin / Ratio: @ratio', [
      '@flowplayer' => $this->getSetting('flowplayer'),
      '@width' => $this->getSetting('width'),
      '@height' => $this->getSetting('height'),
      '@skin' => $this->getSetting('skin'),
      '@ratio' => $this->getSetting('ratio'),
      '@playlist' => $this->getSetting('playlist') ? t(', playlist') : '',
      '@videodesccontains' => $this->getSetting('videodesccontains'),
      '@advance' => $this->getSetting('advance'),
      '@class' => $this->getSetting('class'),
      '@preload' => $this->getSetting('preload'),
      '@autoplay' => $this->getSetting('autoplay') ? t(', autoplay') : '',
      '@muted' => $this->getSetting('muted') ? t(', muted') : '',
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    $files = $this->getEntitiesToView($items, $langcode);

    /**
     * Early opt-out if the field is empty.
     */
    if (empty($files)) {
      return $elements;
    }

    $poster = array();
    $videos = array();
    $subs = array();

    /**
     * Iterate $files and set $elements.
     */
    foreach ($files as $delta => $file) {

      $file_uri = $file->getFileUri();
      $file_url = Url::fromUri(file_create_url($file_uri));
      $file_ext = pathinfo($file_uri, PATHINFO_EXTENSION);

      /**
       * The description is stored on the file field, not on the file entity.
       * The values from the field can be found in $items[$delta].
       * Using $file->_referringItem to access $items[$delta] and extract
       * the description.
       */
      $item = $file->_referringItem;
      $file_description = $item->description;

      /**
       * Splits Poster, Videos and Subtitles.
       */

      if($file_ext == 'gif' || $file_ext == 'jpg') {
        /**
         * Only the first image is loaded as poster.
         */
        if (empty($poster)) {
          $poster = array('url' => $file_url);
        }
      }
      if($file_ext == 'mp4' || $file_ext == 'ogg' || $file_ext == 'webm') {
        array_push($videos, array('src' => $file_url, 'type' => $file_ext, 'description' => $file_description));
      }

      if($file_ext == 'vtt') {
        array_push($subs, array('src' => $file_url, 'label' => $file_description));
      }

      /**
       * Set $elements.
       */
      $elements[$delta] = array(
        '#theme' => 'FlowplayerHTML5Video',
        '#settings' => $this->getSettings(),
        '#poster' => $poster,
        '#videos' => $videos,
        '#subs' => $subs,
      );
    }

    /**
     * Delete all arrays in $elements but the last one
     * that contains all necessary information.
     * Without deletion duplications will occur.
     * It may not be pretty but is
     * simple, readable, short and it works.
     */
    $elements_count =  count($elements);
    for($i = 0; $i <= $elements_count - 2; $i++) {
      unset($elements[$i]);
    }

    return $elements;
  }
}
