<?php

/**
 * @file
 * Contains \Drupal\flowplayer_html5_video\Plugin\Field\FieldFormatter\FlowplayerHTML5VideoFormatterBase.
 */

namespace Drupal\flowplayer_html5_video\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Base class for FlowplayerHTML5VideoFormatter file formatters.
 */
abstract class FlowplayerHTML5VideoFormatterBase extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    return parent::getEntitiesToView($items, $langcode);
  }
}
